# Calculates the total cost of the meal including the tax and tip
meal = 53.48

tax = 0.07
tip = 0.18

total = meal + (meal * tax) + (meal * tip)

print('$' + str(total))


# Above is how I did it based on the instructions, below is how I would prefer to do it
# meal = 53.48
#
# tax_rate = 0.07
# tip_percent = 0.18
#
# tax = meal * tax_rate
# tip = meal * tip_percent
#
# total = meal + tax + tip
#
# print('$' + str(total))
