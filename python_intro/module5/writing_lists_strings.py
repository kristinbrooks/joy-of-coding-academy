# Practice: Writing Lists & Strings

# 1: Write a program that creates a list of 20 numbers input by the user and prints the average
#    (mean) of the list.
# nums = []
# for i in range(20):
#     nums.append(float(input('Enter a number: ')))
# average = sum(nums) / len(nums)
# print('The average of your numbers is:', average)


# 2: Write a function mangle that takes a string as a parameter and returns the string after
#    performing the following operations:
#        i.   Converting the string to all upper case letters
#        ii.  Removing the third character
#        iii. Removing the third to last character
#    Test that your function works

#       Example function calls:	                    Output:
#           print(mangle(“hellothere”))	                HELOTHRE
#           print(mangle(“42 degrees Celsius”))	        42DEGREES CELSUS
#           print(mangle(“eeeeeee”))	                EEEEE
def mangle(string):
    upper_str = string.upper()
    str_remove_char1 = upper_str[:2] + upper_str[3:]
    str_remove_char2 = str_remove_char1[:-3] + str_remove_char1[-2:]
    return str_remove_char2

    # SOLUTIONS version
    # def mangle(string):
    #     string = string.upper()
    #     return string[:2] + string[3:-3] + string[-2:]


print(mangle('hellothere'))
print(mangle('42 degrees Celsius'))
print(mangle('eeeeeee'))


# 3: Write a function, count_e, that takes a list of strings as a parameter and returns the total
#    number of upper and lower case e’s (“E” and “e”) in all the strings in the list. Test that
#    your function works with multiple examples.
#
#       Example function call:	                            Output:
#           print(count_e([“hi”, “hello”, “EEK!”]))	            3
def count_e(strs):
    total_e = 0
    for string in strs:
        num_e = string.count('e') + string.count('E')
        total_e += num_e
    return total_e

    # SOLUTIONS version
    # def count_e(list):
    #     tot = 0
    #     for string in list:
    #         tot += string.count(“e”)
    #         tot += string.count(“E”)
    #     return tot


print(count_e(['hi', 'hello', 'EEK!']))
print(count_e(['Kristin', 'JEANNETTE', 'Greg', 'ANNA', 'Eric']))
print(count_e(['New', 'Years', 'Eve!']))


# 4: Write a function, count_vowels, that takes a list of strings as a parameter and returns the
#    total number of upper and lower case vowels (A, E, I, O, U) in all the strings in the list.
#
#       Example function call:	                                Output:
#           print(count_vowels([“hi”, “hello”, “OOF!”]))	        5
def count_vowels(strs):
    # don't want to edit the original list in case it is being used for other things
    # outside this function so making a new list where all the strings are uppercase for counting
    strs_upper = []
    for string in strs:
        strs_upper.append(string.upper())
    total_vowels = 0
    for string in strs_upper:
        num_vowels = string.count('A') + string.count('E') + string.count('I') + \
                     string.count('O') + string.count('U')
        total_vowels += num_vowels
    return total_vowels

    # SOLUTIONS VERSION - way more efficient!
    # def count_vowels(list):
    #     tot = 0
    #     for string in list:
    #         for letter in string:
    #             if letter in “AEIOUaeiou”:
    #                 tot += 1
    #     return tot


print(count_vowels(['hi', 'hello', 'OOF!']))
print(count_vowels(['Kristin', 'JEANNETTE', 'Greg', 'ANNA', 'Eric']))
print(count_vowels(['New', 'Years', 'Eve!']))
