# Challenge: Word Problem
#
# Your friend is overwhelmed with the number of candidates running in a local election and is
# undecided on whom to vote for.  Write a program to help him decide. Your friend admires
# experience—at least 5 years—but not too much experience that the candidate is disconnected from
# the American people (no more than 20 years)—and wants a candidate who agrees with him on at least
# 80% of the important issues. Prompt the user to input the candidate’s number of years of
# experience and the percentage agreement on important issues.
#
# Example runs:
#
# > python3 vote.py
# Enter the candidate’s years of experience: 1
# What percentage of the issues do you and the candidate agree on? 100
# Do not vote for this candidate
#
# > python3 vote.py
# Enter the candidate’s years of experience: 5
# On what percentage of the issues do you and the candidate agree? 86
# Vote for this candidate
#
# > python vote.py
# Enter the candidate’s years of experience: 21
# On what percentage of the issues do you and the candidate agree? 90
# Do not vote for this candidate
#
# > python vote.py
# Enter the candidate’s years of experience: 20
# On what percentage of the issues do you and the candidate agree? 80
# Vote for this candidate

exp = int(input('Enter the candidate’s years of experience: '))
issues = int(input('On what percentage of the issues do you and the candidate agree? '))

if 5 <= exp <= 20 and issues >= 80:
    print('Vote for this candidate')
else:
    print('Do not vote for this candidate')
