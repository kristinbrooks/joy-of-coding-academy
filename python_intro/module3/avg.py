# Calculates and prints the average of 10 real (i.e., decimal) numbers entered by the user.
# The user will hit enter after each number.

total = 0

for i in range(10):
    total += float(input('Please enter a number: '))

print('The average of your numbers is:', total / 10)
