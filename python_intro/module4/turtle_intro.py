import turtle
turtle.shape("turtle")
turtle.speed(0)


def square(side):
    for x in range(4):
        turtle.forward(side)
        turtle.left(90)


def rectangle(width, height):
    for x in range(2):
        turtle.forward(width)
        turtle.left(90)
        turtle.forward(height)
        turtle.left(90)


turtle.pensize(8)
turtle.pencolor("black")
turtle.penup()

# move turtle away from center to draw frame
turtle.back(200)
turtle.right(90)
turtle.forward(200)
turtle.left(90)
turtle.pendown()

# draw frame
square(400)

# move turtle back to center to draw design
turtle.penup()
turtle.left(90)
turtle.forward(200)
turtle.right(90)
turtle.forward(200)

# draw design
turtle.pensize(5)
turtle.pencolor("purple")
turtle.pendown()
for i in range(6):
    rectangle(50, 150)
    turtle.left(60)
turtle.right(30)
turtle.pencolor("blue")
for i in range(6):
    rectangle(90, 50)
    turtle.right(60)

turtle.Screen().exitonclick()
