# Get a number as input from the user & output:
#   if the number is a multiple of 3, print "Multiple of three!"
#   if the number is even, call hello
#   if the number is odd, call goodbye

def hello(num):
    print("Hello, number", num)


def goodbye(num):
    print("Goodbye, number", num)


number = int(input('Enter a number: '))

if number % 3 == 0:
    print('Multiple of three!')
if number % 2 == 0:
    hello(number)
else:
    goodbye(number)
