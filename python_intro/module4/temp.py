# Asks the user for the current temperature and outputs what they should wear.

temp = int(input('What’s the current temperature? '))

if temp > 90:
    print('Whoa, it’s boiling!')
elif temp >= 80:
    print('It’s getting hot!')
elif temp >= 60:
    print('A perfect day')
elif temp >= 32:
    print('Don’t forget your sweater')
else:
    print('Brrr, you need a coat!')
