# takes the length of the longest side as a parameter and draws a successively smaller rectangle
# using a loop. The short side of the rectangle should be half of the longest side length. The
# minimum side should be of length 6, with 10 pixels in between each rectangle.

import turtle
turtle.shape("turtle")
turtle.speed(0)
turtle.color("red")
turtle.pensize(2)

# my function
# def rectangle(long_side):
#     short_side = long_side * 0.5
#     if short_side < 6:
#         return
#     for i in range(2):
#         turtle.forward(long_side)
#         turtle.right(90)
#         turtle.forward(short_side)
#         turtle.right(90)
#     rectangle(long_side - 20)


# function from the solution document
def rectangle(side):
    for s in range(6, side + 1, 10):
        for i in range(2):
            turtle.forward(s)
            turtle.right(90)
            turtle.forward(s//2)
            turtle.right(90)


rectangle(200)

turtle.Screen().exitonclick()
