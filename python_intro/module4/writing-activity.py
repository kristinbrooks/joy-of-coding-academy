# Write a function space that takes the following four parameters:
# - num_space: the number of spaces in each segment
# - num_seg: the number of segments
# - space_char: the character used for each space
# - seg_char: the character used for each segment.
#
# and outputs the following for the given calls:
# Example call                   Prints
# space(2, 7, "-", "+")    +--+--+--+--+--+--+--+
# space(7, 3, ".", "*")    *.......*.......*.......*
# space(5, 4, " ", "|")    |     |     |     |     |

# Write a function discriminant that takes 3 integers a, b, & c as parameters and returns the result
# under the square root of the quadratic formula:
#
# In other words, return the square root of b squared minus 4 times a times c. If 4ac > b2, then
# return 0.
#
# Example call                  Returns
# discriminant(4, 5, 6)         0
# discriminant(2, -4, 10)       0
# discriminant(6, 15, 3)        12.36931687685298
# discriminant(4, 10, -2)       11.489125293076057

from math import sqrt


def test_helper(actual, expected):
    if actual == expected:
        print(actual, "PASSED")
    else:
        print("ERROR", actual, "!=", expected)


def space(num_space, num_segment, space_char, seg_char):
    print((seg_char + (space_char * num_space)) * num_segment, end='')
    print(seg_char)


def discriminant(a, b, c):
    if 4 * a * c > b ** 2:
        return 0
    return sqrt(b ** 2 - 4 * a * c)


def main():
    space(2, 7, "-", "+")
    space(7, 3, ".", "*")
    space(5, 4, " ", "|")
    test_helper(discriminant(4, 5, 6), 0)
    test_helper(discriminant(2, -4, 10), 0)
    test_helper(discriminant(6, 15, 3), 12.36931687685298)
    test_helper(discriminant(4, 10, -2), 11.489125293076057)


main()
