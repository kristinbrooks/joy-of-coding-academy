# asks the user for 2 parts of speech as input (a plural noun and an adjective) and outputs the
# following:
#
# ______ are red, violets are blue
# Monty Python is _______, woo hoo!

noun = input("Please enter a plural noun: ")
adjective = input("Please enter an adjective: ")

print(noun.capitalize(), "are Red, violets are blue")
print("Monty Python is " + adjective.lower() + ", woo hoo!")
