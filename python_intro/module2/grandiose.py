# Get a synonym for 'faster' from the user and use the adjective they input when you respond that
# your laptop is so much faster than theirs, you are surprised that their laptop can… (fill in the
# blank).

synonym = input("PLease enter a synonym for the word 'faster': ")
print("My computer is so much", synonym, "than yours that I'm surprised yours can even open a web "
                                         "browser!")
