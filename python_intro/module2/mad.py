# Asks the user to enter a phrase, and then prints out the following:
#     a) The “mad” version of the phrase in all caps.
#     b) A confused (i.e., mad) version of the phrase where all letter e’s are replaced by x’s.
#     c) The maddest of all: print whether the phrase is printable.

phrase = input("Please enter a phrase: ")

mad_phrase = phrase.upper()
confused_phrase = phrase.replace("e", "x")

print("The \"mad\" version of your phrase is \"" + mad_phrase + "\"")
print("The \"confused\" version of your phrase is \"" + confused_phrase + "\"")
print("It is", phrase.isprintable(), "that your phrase is printable.")
