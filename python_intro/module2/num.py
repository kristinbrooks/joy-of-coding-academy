# asks the user for their name and their favorite number as input and outputs the following:
#     a) The user’s name followed by “ ’s number” centered (assume 80 characters width) and all in
#         title case.
#     b) Whether or not the number is a decimal number
#     c) The number of 1’s in the number
#     d) The factorial of the absolute number rounded to 0 decimals
#     e) The log base 5 of the absolute value of the number

from math import factorial
from math import log

name = input("Please enter your name: ")
fav_num = input("Please enter your favorite number: ")

# Make the name title case and strip any surrounding whitespace entered by the user
title_text = name.title().strip() + "'s Number"

# Get needed formats of the favorite number
abs_num = abs(int(fav_num))
rounded_abs_num = round(abs_num, 0)

print(title_text.center(80))
print("Is decimal?", fav_num.isdecimal())
print("How many 1's?", fav_num.count('1'))
print("Factorial:", factorial(rounded_abs_num))
print("Log base 5:", log(abs_num, 5))
