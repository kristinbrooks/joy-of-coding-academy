# Asks the user for their favorite month
# and day of the month as input and outputs the following:
#     a) The month & day followed by “ , 2020” with proper capitalization and spacing
#         (i.e., no space before the comma, one space after).
#     b) Whether or not the month is alphanumeric
#     c) The number of 2’s in the number
#     d) The factorial of the number
#     e) The log base 2 of the absolute value of the number

from math import factorial
from math import log

month = input('Please enter your favorite month: ')
str_day = input('Enter your favorite day of the month: ')
int_day = int(str_day)

formatted_date = month.capitalize() + ' ' + str_day + ', 2020'

print(formatted_date)
print('Is the month alphanumeric?', month.isalnum())
print('How many 2\'s?', str_day.count('2'))
print('Factorial:', factorial(int_day))
print('log base 2:', log(abs(int_day), 2))
