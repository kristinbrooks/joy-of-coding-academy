# Gets a movie rating from a user and prints the difference between their rating and mine

my_rating = 5
user_rating = int(input("Enter your rating (1-5 stars) for the movie 'The Fifth Element': "))

rating_difference = str(my_rating - user_rating)
print("The difference between our ratings is", rating_difference + ".")
