# Takes a decimal number as input from the user and prints out the following:
#   a) The absolute value of the number
#   b) The number rounded to 0 decimal places
#   c) The square root of the rounded number’s absolute value

from math import sqrt

number = float(input("Please enter a number: "))

abs_num = abs(number)
rounded_num = round(number, 0)
abs_rounded_num = abs(rounded_num)
sqrt_num = sqrt(abs_rounded_num)

print(abs_num)
print(rounded_num)
print(sqrt_num)
