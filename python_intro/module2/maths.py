# Takes a number as input from the user and prints out the following information:
#   a) The absolute value of the number.
#   b) The floor of the number using the math module (e.g., floor(3.75) is 3).
#   c) The number rounded to 2 decimal places.

import math

number = float(input("Enter a number: "))

print("The absolute value of", number, "is", abs(number))
print("The floor of", number, "is", math.floor(number))
print(number, "rounded to 2 decimal places is", round(number, 2))
