# Name: Kristin Brooks
# Activity: Problem-Solving
#
# Write a function count_uniq that takes a string of sorted
# numbers as a parameter and returns the number of unique numbers using a dictionary.
#
# Example function call(s):             Output:
# print(count_uniq(“111224446”))        4
# print(count_uniq(“30444775555”))      5
# print(count_uniq(“”))                 0

def count_uniq(str):
    count_occs = {}
    for num in str:
        if num in count_occs:
            count_occs[num] += 1
        else:
            count_occs[num] = 1
    return len(count_occs)


print(count_uniq('111224446'))
print(count_uniq('30444775555'))
print(count_uniq(''))
