# Name:     Kristin Brooks
# Activity: Arrays & Files
#
# Assignment:
# 1. Manually download the file turing.txt:
# http://emhill.github.io/150/morea/10.dict//data/turing.txt & save to your computer or
# upload to repl.it.
#
# 2. Create a new program in your favorite editor. Create an array (or list) in your program to
# store each line of the file as a separate element.
#
# 3. Open the file, read in each line into your array/list, and close the file.
#
# 4. Print out the first & last two elements of your array/list, and the number of lines in the
# file.
#
# 5. Your program should not take any user input.

turing_text = []
file = open('turing.txt', 'r')
for line in file:
    turing_text.append(line.rstrip())
file.close()
print(f'First two lines:\n{turing_text[:2]}')
print(f'Last two lines:\n{turing_text[-2:]}')
print(f'Number of lines: {len(turing_text)}')
