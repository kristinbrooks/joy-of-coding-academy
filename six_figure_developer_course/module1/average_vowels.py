# def average_vowels(str_list):
#     count = 0
#     vowels = 'aeiou'
#     for vowel in vowels:
#         for str in str_list:
#             count += str.lower().count(vowel)
#     ave = count // len(str_list)
#     return ave


# Note: works for test case, but fails if there are multiple of the same vowel in the same word
# because we are always only incrementing count by 1 when we find a vowel
def average_vowels(str_list):
    vowels = 'AEIOUaeiou'
    count = 0
    i = 0
    while i < len(vowels):
        for str in str_list:
            if vowels[i] in str:
                count += 1
        i += 1
    ave = count // len(str_list)
    return ave


list1 = ['Zebra', 'lightsaber', '1234 JOYS!']
print(average_vowels(list1))
