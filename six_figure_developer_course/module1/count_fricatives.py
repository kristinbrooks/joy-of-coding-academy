def count_fricatives(str_list):
    fricatives = ['f', 's', 'v', 'z']
    count = 0
    for fric in fricatives:
        i = 0
        while i < len(str_list):
            current_str = str_list[i].lower()
            count += current_str.count(fric)
            i += 1
    return count


list1 = ['Zebra', 'lightsaber', '1234 JOYS!']
list2 = ['Zebra', 'lightsaber', '1234 JOYS!', 'fashion', 'velcro', 'zipper']
print(count_fricatives(list1))
print(count_fricatives(list2))
