# DATA PROJECT
#
# Assignment: Write a program that reads in the csv file linked above and outputs the mean, median,
# and standard deviation for the fall & spring semesters. Make sure to write at least two functions
# in your final solution.
#
# Sample Output:
#           Fall 2016      Spring 2016
# Mean:        88.38           85.86
# Median:      88.5            87.0
# STD:          6.29           10.17

from statistics import mean, median, stdev


def get_data(file_name):
    all_data = []
    for line in open(file_name):
        line_data = tuple(line.rstrip().split(','))
        all_data.append(line_data)
    return all_data


def sort_grades_by_semester(file_name):
    all_data = get_data(file_name)
    fall = ()
    spring = ()
    for data in all_data:
        if 'Fall 2016' in data:
            fall += (int(data[2]),)
        else:
            spring += (int(data[2]),)
    return [fall, spring]


def calculate_stats(nums_list):
    ave = round(mean(nums_list), 2)
    med = round(median(nums_list), 2)
    st_dev = round(stdev(nums_list), 2)
    stats = (ave, med, st_dev)
    return stats


def main():
    semester_grades = sort_grades_by_semester('sample_grades.csv')
    fall_stats = calculate_stats(semester_grades[0])
    spring_stats = calculate_stats(semester_grades[1])

    print(' ' * 10, 'Fall 2016', ' ' * 5, 'Spring 2016')
    print('Mean:', ' ' * 6, fall_stats[0], ' ' * 11, spring_stats[0])
    print('Median:', ' ' * 4, fall_stats[1], ' ' * 12, spring_stats[1])
    print('STD:', ' ' * 8, fall_stats[2], ' ' * 11, spring_stats[2])


if __name__ == "__main__":
    main()
