# Activity: Strings, Lists & Whiles

# 1. Write a function average_neg_evens that takes a list of numbers as a parameter, and adds all
# the negative even numbers (less than 0 and divisible by 2) together and returns their average.

def average_neg_evens(num_list):
    total = 0
    num_count = 0
    i = 0
    while i < len(num_list):
        if num_list[i] < 0 and num_list[i] % 2 == 0:
            total += num_list[i]
            num_count += 1
        i += 1
    average = total // num_count
    return average


# 2. Write a function count_letter that takes a list of strings and a string letter as parameters
# and returns the number of times this letter occurs, both upper- & lower-cased.


def count_letter(str_list, letter):
    new_str = ''
    for str in str_list:
        new_str += str.lower()
    occurances = new_str.count(letter)
    return occurances


def main():
    print(average_neg_evens([0, 1, 2, -2, -3, -4, 3, 4]))
    print(average_neg_evens([-4, 1, -2, -2, -3, -4, 3, 4, 6, -7, -8]))
    print(average_neg_evens([0, -1, 2, -2, -3, 4, -3, 4, -5, -10, -7]))
    print()

    list = ["HELLO", "goodbye", "1234 Oooh!"]
    print(count_letter(list, "o"))


if __name__ == "__main__":
    main()
