def count_lines(file_name):
    file_data = open(file_name, 'r')
    count = len(file_data.readlines())
    file_data.close()
    return count


def count_lines_mult_files(files_list):
    file_counts = []
    for file in files_list:
        count = count_lines(file)
        new_element = [file, count]
        file_counts.append(new_element)
    return file_counts


def write_line_counts_file(files_list, file_name):
    file_counts = count_lines_mult_files(files_list)
    i = 0
    outfile = open(file_name, 'w')
    while i < len(file_counts):
        new_line = f'{file_counts[i][0]} : {file_counts[i][1]}'
        if i < len(file_counts) - 1:
            new_line = new_line + '\n'
        outfile.write(new_line)
        i += 1
    outfile.close()


def main():
    text_files = ['text1.txt', 'text2.txt', 'text3.txt']
    write_line_counts_file(text_files, 'counts.txt')


if __name__ == "__main__":
    main()
