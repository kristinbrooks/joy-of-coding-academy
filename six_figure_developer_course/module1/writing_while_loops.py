# 1 - Write while loops (desired outputs shown in doc for assignment
# a
count = 1
while count <= 5:
    print(count)
    count += 1
print()
# b
i = 2
while i < 12:
    print(i)
    i += 3
print()

# c
num = -10
while num <= 0:
    print(num, end=" ")
    num += 2
print()

# d
x = 0
while x < 4:
    print('****')  # or print(4 * "*")
    x += 1
print()

# 2 - Write a while loop that prints the letters of "CSCI 150" on separate lines
string = "CSCI 150"
c = 0
while c < len(string):
    if string[c].isalpha():
        print(string[c])
    c += 1
print()

# solution doc does it this way, but I don't consider that to be what was asked for...it only
# said to print the letters

# while c < len(string):
#     print(str[c])
#     c += 1

# 3 Create a program that allows the user to enter in a list of numbers, prints them out in sorted
# order, and prints the sum and average of the numbers. Prompt the user to continue entering
# numbers, and enter the number ‘0’ when finished.
nums = []
while True:
    number = int(input('Enter a number or "0" if finished: '))
    if number == 0:
        break
    nums.append(number)
print('Sorted numbers:', sorted(nums))
print('Sum:', sum(nums))
print('Average:', sum(nums)/len(nums))
