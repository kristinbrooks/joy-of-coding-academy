from statistics import median, mean

# 1. Using a while loop, write a program that prints out the even numbers from 2 to 20.

# num = 2
# while num <= 21:
#     print(num)
#     num += 2

# 2. Write a program that gets a string as input from the user, and finds the index of the first
# occurrence of the letter ‘e’ (upper or lower case). Your program should define a function that
# takes a string as a parameter and returns the e index. If the string does not contain an e, the
# function should return -1.

# Example call		Returns
# find_e(“hello”)	 1
# find_e(“how”)		-1


# def find_e(str):
#     index = str.lower().find('e')
#     return index
#
#
# user_string = input('Enter a string: ')
# e_location = find_e(user_string)
# print("Index of first 'e': {}".format(e_location) if e_location > -1 else "No 'e' in string")

# 3. Create a program that allows the user to enter a list of strings, prints them out in sorted
# order, and prints the number of strings entered as well as the total number of characters in all
# the strings in the list. Prompt the user to continue entering phrases, and enter ‘Q’ when
# finished. If the user enters an empty string, output an informative error message.
# Test your program.
#
# Example program run:
# > python3 count_strings.py
# > Please enter a phrase (‘Q’ to finish) hello
# > Please enter a phrase (‘Q’ to finish) goodbye
# > Please enter a phrase (‘Q’ to finish)
# > Please enter non-empty strings or ‘Q’ to quit.
# > Please enter a phrase (‘Q’ to finish) q
# > Please enter a phrase (‘Q’ to finish) Q
# > You entered 4 strings and 29 characters.

# user_str = ''
# str_list = []
# total_characters = 0
# while True:
#     user_str = input('Please enter a phrase (\'Q\' to finish): ')
#     if user_str == '':
#         print('Please enter non-empty strings or \'Q\' to quit.')
#         continue
#     elif user_str == 'Q':
#         str_list.append(user_str)
#         break
#     str_list.append(user_str)
#
# num_strings = len(str_list)
# for string in str_list:
#     total_characters += len(string)
#
# print(sorted(str_list))
# print('You entered {} strings and {} characters'.format(num_strings, total_characters))

# 4. Write a program that reads in a file of strings (one per line), creates a new file,
# and prints out each line followed by the number of characters in each line. At the end of the new
# file your program should print the total and average number of characters per line. Test that
# your program works with the following file of strings (you need to create this file in PyCharm):
#
#
# Top 10 Movies of 2015:
# Star Wars Episode VII: The Force Awakens
# Jurassic World
# The Avengers: Age of Ultron
# Inside Out
# Furious 7
# American Sniper
# Minions
#
# The Hunger Games: Mockingjay – Part 2
# The Martian
# Cinderella
#
#
# Creates the following output file:

# Top 10 Movies of 2015 : 21
# Star Wars Episode VII: The Force Awakens : 40
# Jurassic World : 14
# The Avengers: Age of Ultron : 27
# Inside Out : 10
# Furious 7 : 9
# American Sniper : 15
# Minions : 7
#  : 0
# The Hunger Games: Mockingjay – Part 2 : 37
# The Martian : 11
# Cinderella : 10
# 201 characters and 16.75 characters per line

# movie_file = open('movie-list.txt')
# movie_list = movie_file.readlines()
# movie_file.close()
#
# lines = len(movie_list)
# line_num = 0
# current_line = ''
# new_line = ''
# characters = 0
#
# list_with_counts = open('movie_list_with_counts.txt', 'a')
# while line_num < lines:
#     current_line = movie_list[line_num].strip('\n')
#     new_line = current_line + ' : ' + str(len(current_line)) + '\n'
#     list_with_counts.write(new_line)
#     characters += len(current_line)
#     line_num += 1
# list_with_counts.write(f'{characters} characters and {characters/lines} characters per line')
# list_with_counts.close()

# 5. Write a program that reads in a file of numbers (one per line) and prints out the sum, mean,
# and median of the numbers. Test that your program works with the following files of numbers:
#
# 3             3
# 30            30
# 4             4
# 0             0
# 0             0
# 4             45
# 45            25
# 25            23
# 23            2
# 2
#
#
# Outputs for first file of numbers:
# Sum: 136
# Mean: 13.6
# Median 4.0
#
# Outputs for second file of numbers:
# Sum: 132
# Mean: 14.666666666666666
# Median 4


# def basic_stats(num_list):
#     num_elements = len(num_list)
#     total = 0
#     ind = 0
#     while ind < num_elements:
#         num_list[ind] = int(num_list[ind].strip('\n'))
#         total += num_list[ind]
#         ind += 1
#
#     print(f'Sum: {total}')
#     print(f'Mean: {mean(num_list)}')
#     print(f'Median: {median(num_list)}')
#
#
# file1 = open('num_list1.txt')
# num_list1 = file1.readlines()
# file1.close()
# print('Outputs for first file of numbers:')
# basic_stats(num_list1)
# print()
#
# file2 = open('num_list2.txt')
# num_list2 = file2.readlines()
# file2.close()
# print('Outputs for second file of numbers:')
# basic_stats(num_list2)

# CHALLENGE EXERCISES:

# 1. Implement & test the function mix_lists1(list1, str2) which
# returns a combined string. E.g.:
# (Using abbreviated list format below. Assume each letter is it’s own list element:
# [he] = [‘h’, ‘e’])
#
# mix_lists([hello], [there]) returns [htehlelroe]
# mix_lists([1234], [abcd]) returns [1a2b3c4d]
# mix_lists([12], [abcd]) returns [1a2bcd]
# mix_lists([1234], [ab]) returns [1a2b34]

list1 = ['h', 'e', 'l', 'l', 'o']
list2 = ['t', 'h', 'e', 'r', 'e']
list3 = [1, 2]
list4 = [1, 2, 3, 4]
list5 = ['a', 'b']
list6 = ['a', 'b', 'c', 'd']


def mix_lists(first_list, second_list):
    new_list = []
    i = 0
    while i < len(first_list) or i < len(second_list):
        if i < len(first_list):
            new_list.append(first_list[i])
        if i < len(second_list):
            new_list.append(second_list[i])
        i += 1
    return new_list


print(mix_lists(list1, list2))
print(mix_lists(list4, list6))
print(mix_lists(list3, list6))
print(mix_lists(list4, list5))

# 2. Implement & test the function
# mix_files(fname1, fname2, outfile) which returns a combined
# string. E.g.:
#
# fname1:
# 1
# 2
# 3
# 4
#
# fname2:
# aa
# bb
# cc
# dd
#
# outfile:
# 1
# aa
# 2
# bb
# 3
# cc
# 4
# dd


def get_list_from_file(fname):
    file = open(fname)
    file_list = file.readlines()
    file.close()
    return file_list


def mix_files(fname1, fname2, outfile):
    first_list = get_list_from_file(fname1)
    second_list = get_list_from_file(fname2)
    new_file = open(outfile, 'w')
    for i in range(len(first_list)):
        if i < len(first_list) - 1:
            new_file.write(first_list[i])
            new_file.write(second_list[i])
        else:
            new_file.write(first_list[i] + '\n')
            new_file.write(second_list[i])
    new_file.close()


mix_files('nums.txt', 'letters.txt', 'mixed_file.txt')
