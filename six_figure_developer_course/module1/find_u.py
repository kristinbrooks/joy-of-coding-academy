# def find_u(usr_str):
#     i = 0
#     while i < len(usr_str):
#         if usr_str[i].lower() == 'u':
#             return i
#         i += 1
#     return -1


def find_u(usr_str):
    i = 0
    while i < len(usr_str):
        if usr_str[i] == 'u' or usr_str[i] == 'U':
            return i
        i += 1
    return -1


def main():
    usr_input = input("Enter a string: ")
    print(find_u(usr_input))


if __name__ == "__main__":
    main()
