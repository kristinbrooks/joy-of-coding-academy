def count_zeroes():
    prompt = "Enter a number ('Q' to quit)? "
    num = ''
    count = 0
    while num != 'Q':
        num = input(prompt)
        count += num.count('0')

    print(f'You entered {count} zeroes')


count_zeroes()
