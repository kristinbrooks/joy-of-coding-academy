def word_count(text):
    words = text.split()
    count = len(words)
    return count


def file_counts(file_name):
    file_data = open(file_name, 'r')
    w_count = 0
    l_count = 0
    for line in file_data:
        w_count += word_count(line)
        l_count += 1
    file_data.close()
    return [l_count, w_count]


def count_mult_files(files_list):
    f_counts = []
    for file in files_list:
        counts = file_counts(file)
        new_element = [file, counts[0], counts[1]]
        f_counts.append(new_element)
    return f_counts


def write_total_counts_file(files_list, file_name):
    f_counts = count_mult_files(files_list)
    total_lines = 0
    total_words = 0
    outfile = open(file_name, 'w')
    for file in f_counts:
        file_name = file[0]
        l_count = file[1]
        w_count = file[2]
        new_line = f'{file_name} : {l_count} lines, {w_count} words\n'
        outfile.write(new_line)
        total_lines += l_count
        total_words += w_count
    totals = f'TOTAL: {total_lines} lines, {total_words} words'
    outfile.write(totals)
    outfile.close()


def main():
    text_files = ['text1.txt', 'text2.txt', 'text3.txt']
    write_total_counts_file(text_files, 'total_counts.txt')


if __name__ == "__main__":
    main()
