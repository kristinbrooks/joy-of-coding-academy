usr_file = input("Enter the name of your file: ")
out_file = open(usr_file, 'w')
prompt = 'Enter a number for your file (or \'0\' to quit): '
num = int(input(prompt))
while num != 0:
    print(num, file=out_file)
    num = int(input(prompt))
out_file.close()
